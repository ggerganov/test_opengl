/*****************************************************************************
 *
 * File Name : test1.cpp
 *
 * Creation Date : 17-08-2012
 *
 * Last Modified : Fri 17 Aug 2012 17:38:22 CEST
 *
 *****************************************************************************/

/*! \file test1.cpp
 *  \brief Enter description here.
 *  \author Georgi Gerganov
 */

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/glx.h>
#include "glm/glm.hpp"

#include <iostream>

#define REFRESH_DELAY 10 // ms

void *font = GLUT_BITMAP_8_BY_13;

float H_ANGLE_STEP = 0.2f;
float V_ANGLE_STEP = 0.2f;

float g_horizontal_angle = 0;
float g_vertical_angle = 0;

int mouse_old_x = 400;
int mouse_old_y = 300;

using namespace std;

void mouse(int button, int state, int x, int y);
void motion(int, int);
void updateCamera();

GLfloat verts[] = {-1, -1, -0,  
                    1, -1, -0,
                    0,  1, -0
                  };
GLubyte indeces[] = {0, 2, 1};

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glColor3f(1.0, 1.0, 1.0);

  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, verts);
  glDrawArrays(GL_TRIANGLES, 0, 3);
  //glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_BYTE, indeces);
  glDisableClientState(GL_VERTEX_ARRAY);

  glPushMatrix();                     // save current modelview matrix
  glLoadIdentity();                   // reset modelview matrix
    
  glMatrixMode(GL_PROJECTION);        // switch to projection matrix
  glPushMatrix();                     // save current projection matrix
  glLoadIdentity();                   // reset projection matrix
  gluOrtho2D(0, 800, 600, 0); // set to orthogonal projection

  glColor4f(1, 1, 0, 0.5);
  glRasterPos2i(10, 10);
  glutBitmapCharacter(font, 'A');
  glutBitmapCharacter(font, 'h');
  glutBitmapCharacter(font, 'a');

  glPopMatrix();                   // restore to previous projection matrix

  glMatrixMode(GL_MODELVIEW);      // switch to modelview matrix
  glPopMatrix();                   // restore to previous modelview matrix

  glutSwapBuffers();
}

void timerEvent(int value) {
  glutPostRedisplay();
  glutTimerFunc(REFRESH_DELAY, timerEvent, 0);
}

void init() {
  glEnable(GL_CULL_FACE);

  // set perspective viewing frustum
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //glOrtho(-4, 4, -3, 3, 1, 10);
  gluPerspective(45.0f, (float)(800)/600, 1.0f, 1000.0f); // FOV, AspectRatio, NearClip, FarClip

  updateCamera();

  glutDisplayFunc(display);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutTimerFunc(REFRESH_DELAY, timerEvent, 0);
}

void updateCamera() {
  //cout << g_vertical_angle << " " << g_horizontal_angle << endl;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(0.0, 0.0, -10.0);
  glRotatef(g_vertical_angle, 1.0, 0.0, 0.0);
  glRotatef(g_horizontal_angle, 0.0, 1.0, 0.0);
  return ;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glm::vec3 dir(cos(g_vertical_angle) * sin(g_horizontal_angle),
                sin(g_vertical_angle),
                cos(g_vertical_angle) * cos(g_horizontal_angle));

  glm::vec3 right(sin(g_horizontal_angle) - 3.1415f/2.0f,
                  0,
                  cos(g_horizontal_angle) - 3.1415f/2.0f);

  glm::vec3 top = glm::cross(right, dir);

  gluLookAt(0, 0, 0,
            dir.x, dir.y, dir.z,
            top.x, top.y, top.z);
}

void mouse(int button, int state, int x, int y) {
  if (state == GLUT_DOWN) {
  }

  mouse_old_x = x;
  mouse_old_y = y;
}

void motion(int x, int y) {
  float dx, dy;

  dx = (float)(x - mouse_old_x);
  dy = (float)(y - mouse_old_y);

  g_horizontal_angle += dx * H_ANGLE_STEP;
  g_vertical_angle   += dy * V_ANGLE_STEP;

  mouse_old_x = x;
  mouse_old_y = y;

  updateCamera();
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(800, 600);
  int iGLUTWindowHandle = glutCreateWindow("Test1");
  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

  init();

  glutMainLoop();

  return 0;
}
