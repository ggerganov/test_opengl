/*****************************************************************************
 *
 * File Name : test1.cpp
 *
 * Creation Date : 17-08-2012
 *
 * Last Modified : Mon 20 Aug 2012 12:48:13 AM EEST
 *
 *****************************************************************************/

/*! \file test1.cpp
 *  \brief Enter description here.
 *  \author Georgi Gerganov
 */

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/glx.h>
#include "glm/glm.hpp"

#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

#define REFRESH_DELAY 200 // ms

void *font = GLUT_BITMAP_8_BY_13;

float H_ANGLE_STEP = 0.2f;
float V_ANGLE_STEP = 0.2f;

float g_horizontal_angle = 0;
float g_vertical_angle = 0;

int mouse_old_x = 400;
int mouse_old_y = 300;

using namespace std;

void mouse(int button, int state, int x, int y);
void motion(int, int);
void updateCamera();

GLfloat LightAmbient[]  = { 0.3f, 0.3f, 0.3f, 0.5f };
GLfloat LightDiffuse[]  = { 1.0f, 1.0f, 0.5f, 0.5f };
GLfloat LightPosition[]  = { 0.0f, 0.0f, 1.0f, 1.0f };

GLfloat verts[] = {-1, -1, -0,  
                    1, -1, -0,
                    0,  1, -0,
                    0, 0, -2.5
                  };
GLfloat norms[] = {0, -1, 1,
                   0, -1, 1,
                   0,  0, 1,
                   0, -1, 0
                  };
GLubyte indeces[] = {0, 1, 2, 0, 3, 1};

int nfverts = 0;
int nfindeces = 0;
vector<GLfloat> fverts;
vector<GLfloat> fnorms;
vector<GLfloat> fcolors;
vector<GLuint> findeces;

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_LIGHTING);

  //glVertexPointer(3, GL_FLOAT, 0, verts);
  //glNormalPointer(GL_FLOAT, 0, norms);
  //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, indeces);
  //glDrawElements(GL_TRIANGLES, nfindeces, GL_UNSIGNED_INT, &findeces[0]);
  glCallList(1);

  glDisable(GL_LIGHTING);
  
  glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

  glColor4f(0.0, 1.0, 0.0, 0.5f);
  //glDrawElements(GL_TRIANGLES, nfindeces, GL_UNSIGNED_INT, &findeces[0]);
  
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

  /*
  glPushMatrix();                     // save current modelview matrix
  glLoadIdentity();                   // reset modelview matrix
    
  glMatrixMode(GL_PROJECTION);        // switch to projection matrix
  glPushMatrix();                     // save current projection matrix
  glLoadIdentity();                   // reset projection matrix
  gluOrtho2D(0, 800, 600, 0);         // set to orthogonal projection

  glDisable(GL_LIGHTING);
  glColor4f(1, 1, 0, 0);
  glRasterPos2i(10, 10);
  glutBitmapCharacter(font, 'A');
  glutBitmapCharacter(font, 'h');
  glutBitmapCharacter(font, 'a');
  glEnable(GL_LIGHTING);

  glPopMatrix();                   // restore to previous projection matrix

  glMatrixMode(GL_MODELVIEW);      // switch to modelview matrix
  glPopMatrix();                   // restore to previous modelview matrix

  */

  glutSwapBuffers();
  
}

void timerEvent(int value) {
  glutPostRedisplay();
  glutTimerFunc(REFRESH_DELAY, timerEvent, 0);
}

void init() {
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);

  // set perspective viewing frustum
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //glOrtho(-4, 4, -3, 3, 1, 10);
  gluPerspective(30.0f, (float)(800)/600, 1.0f, 1000.0f); // FOV, AspectRatio, NearClip, FarClip

  updateCamera();

  glutDisplayFunc(display);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutTimerFunc(REFRESH_DELAY, timerEvent, 0);

  //glShadeModel(GL_SMOOTH);
  glShadeModel(GL_FLAT);

  glEnable(GL_LIGHTING);
  glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);
  //glLightfv(GL_LIGHT1, GL_POSITION, LightPosition);
  glEnable(GL_LIGHT1);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);

  glVertexPointer(3, GL_FLOAT, 0, &fverts[0]);
  glNormalPointer(GL_FLOAT, 0, &fnorms[0]);
  glColorPointer(3, GL_FLOAT, 0, &fcolors[0]);

  GLuint index = glGenLists(1);
  cout << "index = " << index << endl;

  glNewList(index, GL_COMPILE);
  glDrawElements(GL_TRIANGLES, nfindeces, GL_UNSIGNED_INT, &findeces[0]);
  glEndList();
}

void updateCamera() {
  //cout << g_vertical_angle << " " << g_horizontal_angle << endl;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(0.0, 0.0, -2.0);
  glRotatef(g_vertical_angle, 1.0, 0.0, 0.0);
  glRotatef(g_horizontal_angle, 0.0, 1.0, 0.0);
  return ;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glm::vec3 dir(cos(g_vertical_angle) * sin(g_horizontal_angle),
                sin(g_vertical_angle),
                cos(g_vertical_angle) * cos(g_horizontal_angle));

  glm::vec3 right(sin(g_horizontal_angle) - 3.1415f/2.0f,
                  0,
                  cos(g_horizontal_angle) - 3.1415f/2.0f);

  glm::vec3 top = glm::cross(right, dir);

  gluLookAt(0, 0, 0,
            dir.x, dir.y, dir.z,
            top.x, top.y, top.z);
}

void mouse(int button, int state, int x, int y) {
  if (state == GLUT_DOWN) {
  }

  mouse_old_x = x;
  mouse_old_y = y;
}

void motion(int x, int y) {
  float dx, dy;

  dx = (float)(x - mouse_old_x);
  dy = (float)(y - mouse_old_y);

  g_horizontal_angle += dx * H_ANGLE_STEP;
  g_vertical_angle   += dy * V_ANGLE_STEP;

  mouse_old_x = x;
  mouse_old_y = y;

  updateCamera();
}

void readObj(const char* fname) {
  ifstream fin(fname);
  if (!fin.good()) {
    cerr << "Cannot open file: " << fname << endl;
    return;
  }

  while (!fin.eof()) {
    char line[256];
    fin.getline(line, 256);
    stringstream iss(line);
    string sline;
    iss >> sline;
    if (sline == "v") {
      GLfloat x, y, z;
      iss >> x >> y >> z;
      fverts.push_back(x);
      fverts.push_back(y);
      fverts.push_back(z);

      fnorms.push_back(0);
      fnorms.push_back(0);
      fnorms.push_back(0);

      ++nfverts;
    }
    if (sline == "f") {
      GLushort f1, f2, f3, f4;
      stringstream sf;
      string ids;

      iss >> ids;
      ids.erase(ids.find('/'));
      sf << ids; sf >> f1;

      iss >> ids;
      ids.erase(ids.find('/'));
      sf.clear();
      sf << ids; sf >> f2;

      iss >> ids;
      sf.clear();
      ids.erase(ids.find('/'));
      sf << ids; sf >> f3;

      --f1; --f2; --f3;
      findeces.push_back(f1);
      findeces.push_back(f2);
      findeces.push_back(f3);
      nfindeces += 3;

      glm::vec3 p1(fverts[3*f1+0], fverts[3*f1+1], fverts[3*f1+2]);
      glm::vec3 p2(fverts[3*f2+0], fverts[3*f2+1], fverts[3*f2+2]);
      glm::vec3 p3(fverts[3*f3+0], fverts[3*f3+1], fverts[3*f3+2]);
      glm::vec3 nv = glm::cross(p2-p1, p3-p1);

      fnorms[3*f1+0] += nv.x; fnorms[3*f1+1] += nv.y; fnorms[3*f1+2] += nv.z;
      fnorms[3*f2+0] += nv.x; fnorms[3*f2+1] += nv.y; fnorms[3*f2+2] += nv.z;
      fnorms[3*f3+0] += nv.x; fnorms[3*f3+1] += nv.y; fnorms[3*f3+2] += nv.z;

      iss >> ids;
      if (!iss.eof()) {
        sf.clear();
        ids.erase(ids.find('/'));
        sf << ids; sf >> f4;

        --f4; f2 = f3; f3 = f4;
        findeces.push_back(f1);
        findeces.push_back(f2);
        findeces.push_back(f3);
        nfindeces += 3;

        glm::vec3 p1(fverts[3*f1+0], fverts[3*f1+1], fverts[3*f1+2]);
        glm::vec3 p2(fverts[3*f2+0], fverts[3*f2+1], fverts[3*f2+2]);
        glm::vec3 p3(fverts[3*f3+0], fverts[3*f3+1], fverts[3*f3+2]);
        glm::vec3 nv = glm::cross(p2-p1, p3-p1);

        fnorms[3*f1+0] += nv.x; fnorms[3*f1+1] += nv.y; fnorms[3*f1+2] += nv.z;
        fnorms[3*f2+0] += nv.x; fnorms[3*f2+1] += nv.y; fnorms[3*f2+2] += nv.z;
        fnorms[3*f3+0] += nv.x; fnorms[3*f3+1] += nv.y; fnorms[3*f3+2] += nv.z;
      }
    }
  }

  for (int i = 0; i < nfverts; ++i) {
    glm::vec3 nv(fnorms[3*i+0], fnorms[3*i+1], fnorms[3*i+2]);
    nv = glm::normalize(nv);
    fnorms[3*i+0] = nv.x; fnorms[3*i+1] = nv.y; fnorms[3*i+2] = nv.z;
  }
}

//float iso1 = 18;
float iso1 = 1500;

inline glm::vec3 interp(glm::vec3 p1, glm::vec3 p2, float v1, float v2) {
  return p1 + ((iso1 - v1)/(v2 - v1))*(p2 - p1);
}

void addTriangle(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2) {
  fverts.push_back(p0.x); fverts.push_back(p0.y); fverts.push_back(p0.z);
  fverts.push_back(p1.x); fverts.push_back(p1.y); fverts.push_back(p1.z);
  fverts.push_back(p2.x); fverts.push_back(p2.y); fverts.push_back(p2.z);

  glm::vec3 nv = glm::cross(p1-p0, p2-p0);
  fnorms.push_back(nv.x); fnorms.push_back(nv.y); fnorms.push_back(nv.z);
  fnorms.push_back(nv.x); fnorms.push_back(nv.y); fnorms.push_back(nv.z);
  fnorms.push_back(nv.x); fnorms.push_back(nv.y); fnorms.push_back(nv.z);

  nfverts += 3;

  findeces.push_back(nfindeces+0);
  findeces.push_back(nfindeces+1);
  findeces.push_back(nfindeces+2);
  nfindeces += 3;

  float minx = -0.5;
  float maxx = 0.5;
  float avg = (p0.x + p1.x + p2.x)/3.0;
  float col = (avg - minx) / (maxx-minx);
  fcolors.push_back(col); fcolors.push_back(col); fcolors.push_back(col); 
  fcolors.push_back(col); fcolors.push_back(col); fcolors.push_back(col); 
  fcolors.push_back(col); fcolors.push_back(col); fcolors.push_back(col); 
}

void doTetra(glm::vec3 *p, float *pv, int p0, int p1, int p2, int p3) {
  unsigned char code = 0;
  if (pv[p0] < iso1) code += 1;
  if (pv[p1] < iso1) code += 2;
  if (pv[p2] < iso1) code += 4;
  if (pv[p3] < iso1) code += 8;

  if (code == 0 || code == 15) return;

  if (code == 1) {
    addTriangle(interp(p[p1], p[p0], pv[p1], pv[p0]),
                interp(p[p2], p[p0], pv[p2], pv[p0]),
                interp(p[p3], p[p0], pv[p3], pv[p0]));
  } else if (code == 2) {
    addTriangle(interp(p[p0], p[p1], pv[p0], pv[p1]),
                interp(p[p3], p[p1], pv[p3], pv[p1]),
                interp(p[p2], p[p1], pv[p2], pv[p1]));
  } else if (code == 3) {
    addTriangle(interp(p[p3], p[p0], pv[p3], pv[p0]),
                interp(p[p3], p[p1], pv[p3], pv[p1]),
                interp(p[p2], p[p1], pv[p2], pv[p1]));
    addTriangle(interp(p[p3], p[p0], pv[p3], pv[p0]),
                interp(p[p2], p[p1], pv[p2], pv[p1]),
                interp(p[p2], p[p0], pv[p2], pv[p0]));
  } else if (code == 4) {
    addTriangle(interp(p[p0], p[p2], pv[p0], pv[p2]),
                interp(p[p1], p[p2], pv[p1], pv[p2]),
                interp(p[p3], p[p2], pv[p3], pv[p2]));
  } else if (code == 5) {
    addTriangle(interp(p[p1], p[p0], pv[p1], pv[p0]),
                interp(p[p1], p[p2], pv[p1], pv[p2]),
                interp(p[p3], p[p2], pv[p3], pv[p2]));
    addTriangle(interp(p[p1], p[p0], pv[p1], pv[p0]),
                interp(p[p3], p[p2], pv[p3], pv[p2]),
                interp(p[p3], p[p0], pv[p3], pv[p0]));
  } else if (code == 6) {
    addTriangle(interp(p[p0], p[p1], pv[p0], pv[p1]),
                interp(p[p3], p[p1], pv[p3], pv[p1]),
                interp(p[p0], p[p2], pv[p0], pv[p2]));
    addTriangle(interp(p[p0], p[p2], pv[p0], pv[p2]),
                interp(p[p3], p[p1], pv[p3], pv[p1]),
                interp(p[p3], p[p2], pv[p3], pv[p2]));
  } else if (code == 7) {
    addTriangle(interp(p[p3], p[p0], pv[p3], pv[p0]),
                interp(p[p3], p[p1], pv[p3], pv[p1]),
                interp(p[p3], p[p2], pv[p3], pv[p2]));
  } else if (code == 8) {
    addTriangle(interp(p[p0], p[p3], pv[p0], pv[p3]),
                interp(p[p2], p[p3], pv[p2], pv[p3]),
                interp(p[p1], p[p3], pv[p1], pv[p3]));
  } else if (code == 9) {
    addTriangle(interp(p[p1], p[p0], pv[p1], pv[p0]),
                interp(p[p2], p[p0], pv[p2], pv[p0]),
                interp(p[p1], p[p3], pv[p1], pv[p3]));
    addTriangle(interp(p[p1], p[p3], pv[p1], pv[p3]),
                interp(p[p2], p[p0], pv[p2], pv[p0]),
                interp(p[p2], p[p3], pv[p2], pv[p3]));
  } else if (code == 10) {
    addTriangle(interp(p[p2], p[p1], pv[p2], pv[p1]),
                interp(p[p0], p[p1], pv[p0], pv[p1]),
                interp(p[p0], p[p3], pv[p0], pv[p3]));
    addTriangle(interp(p[p0], p[p3], pv[p0], pv[p3]),
                interp(p[p2], p[p3], pv[p2], pv[p3]),
                interp(p[p2], p[p1], pv[p2], pv[p1]));
  } else if (code == 11) {
    addTriangle(interp(p[p2], p[p0], pv[p2], pv[p0]),
                interp(p[p2], p[p3], pv[p2], pv[p3]),
                interp(p[p2], p[p1], pv[p2], pv[p1]));
  } else if (code == 12) {
    addTriangle(interp(p[p0], p[p3], pv[p0], pv[p3]),
                interp(p[p0], p[p2], pv[p0], pv[p2]),
                interp(p[p1], p[p3], pv[p1], pv[p3]));
    addTriangle(interp(p[p0], p[p2], pv[p0], pv[p2]),
                interp(p[p1], p[p2], pv[p1], pv[p2]),
                interp(p[p1], p[p3], pv[p1], pv[p3]));
  } else if (code == 13) {
    addTriangle(interp(p[p1], p[p0], pv[p1], pv[p0]),
                interp(p[p1], p[p2], pv[p1], pv[p2]),
                interp(p[p1], p[p3], pv[p1], pv[p3]));
  } else if (code == 14) {
    addTriangle(interp(p[p0], p[p3], pv[p0], pv[p3]),
                interp(p[p0], p[p2], pv[p0], pv[p2]),
                interp(p[p0], p[p1], pv[p0], pv[p1]));
  }

/**/
}

void marchingTetrahedrons(const char *fname,
                          const int nx, const int ny, const int nz,
                          const float sx, const float sy, const float sz) {

  float *vol = new float [nx*ny*nz];
  ifstream fin(fname, ios::binary);
  fin.read((char*)(vol), sizeof(float)*nx*ny*nz);
  fin.close();

  glm::vec3 p[8];
  int pi[8];
  float pv[8];
  int step = 16;
  for (float cgz = 0; cgz < nz-1; cgz += 1) {
    cout << cgz << endl;
    for (float cgy = 0; cgy < ny-step; cgy += step) {
      for (float cgx = 0; cgx < nx-step; cgx += step) {
        p[0] = glm::vec3((float)(cgx+0-nx/2)/nx*sx,    (float)(cgy+0-ny/2)/ny*sy, (float)(cgz+0-nz/2)/nz*sz);
        p[1] = glm::vec3((float)(cgx+step-nx/2)/nx*sx, (float)(cgy+0-ny/2)/ny*sy, (float)(cgz+0-nz/2)/nz*sz);
        p[2] = glm::vec3((float)(cgx+0-nx/2)/nx*sx,    (float)(cgy+step-ny/2)/ny*sy, (float)(cgz+0-nz/2)/nz*sz);
        p[3] = glm::vec3((float)(cgx+step-nx/2)/nx*sx, (float)(cgy+step-ny/2)/ny*sy, (float)(cgz+0-nz/2)/nz*sz);
        p[4] = glm::vec3((float)(cgx+0-nx/2)/nx*sx,    (float)(cgy+0-ny/2)/ny*sy, (float)(cgz+1-nz/2)/nz*sz);
        p[5] = glm::vec3((float)(cgx+step-nx/2)/nx*sx, (float)(cgy+0-ny/2)/ny*sy, (float)(cgz+1-nz/2)/nz*sz);
        p[6] = glm::vec3((float)(cgx+0-nx/2)/nx*sx,    (float)(cgy+step-ny/2)/ny*sy, (float)(cgz+1-nz/2)/nz*sz);
        p[7] = glm::vec3((float)(cgx+step-nx/2)/nx*sx, (float)(cgy+step-ny/2)/ny*sy, (float)(cgz+1-nz/2)/nz*sz);

        pi[0] = cgz*ny*nx + cgy*nx + cgx;
        pv[0] = vol[pi[0]];
        pi[1] = pi[0] + step;
        pv[1] = vol[pi[1]];
        pi[2] = pi[0] + step*nx;
        pv[2] = vol[pi[2]];
        pi[3] = pi[2] + step;
        pv[3] = vol[pi[3]];
        pi[4] = pi[0] + nx*ny;
        pv[4] = vol[pi[4]];
        pi[5] = pi[4] + step;
        pv[5] = vol[pi[5]];
        pi[6] = pi[4] + step*nx;
        pv[6] = vol[pi[6]];
        pi[7] = pi[6] + step;
        pv[7] = vol[pi[7]];

        glm::vec3 c1(0.2, 0, 0);
        glm::vec3 c2(-0.1, 0, 0);

        //pv[0] = 2/glm::length(p[0]-c1)+1/glm::length(p[0]-c2);
        //pv[1] = 2/glm::length(p[1]-c1)+1/glm::length(p[1]-c2);
        //pv[2] = 2/glm::length(p[2]-c1)+1/glm::length(p[2]-c2);
        //pv[3] = 2/glm::length(p[3]-c1)+1/glm::length(p[3]-c2);
        //pv[4] = 2/glm::length(p[4]-c1)+1/glm::length(p[4]-c2);
        //pv[5] = 2/glm::length(p[5]-c1)+1/glm::length(p[5]-c2);
        //pv[6] = 2/glm::length(p[6]-c1)+1/glm::length(p[6]-c2);
        //pv[7] = 2/glm::length(p[7]-c1)+1/glm::length(p[7]-c2);

        //pv[1] = glm::length(p[1]);
        //pv[2] = glm::length(p[2]);
        //pv[3] = glm::length(p[3]);
        //pv[4] = glm::length(p[4]);
        //pv[5] = glm::length(p[5]);
        //pv[6] = glm::length(p[6]);
        //pv[7] = glm::length(p[7]);

        //doTetra(p, pv, 0, 2, 3, 7);
        //doTetra(p, pv, 0, 2, 6, 7);
        //doTetra(p, pv, 0, 4, 6, 7);
        //doTetra(p, pv, 0, 6, 1, 2);
        //doTetra(p, pv, 0, 6, 1, 4);
        //doTetra(p, pv, 5, 6, 1, 4);

        doTetra(p, pv, 0, 2, 3, 6);
        doTetra(p, pv, 0, 6, 3, 7);
        doTetra(p, pv, 0, 7, 4, 6);
        doTetra(p, pv, 0, 1, 7, 3);
        doTetra(p, pv, 0, 7, 1, 4);
        doTetra(p, pv, 5, 1, 7, 4);
      }
    }
  }

  for (int i = 0; i < nfverts; ++i) {
    glm::vec3 nv(fnorms[3*i+0], fnorms[3*i+1], fnorms[3*i+2]);
    nv = glm::normalize(nv);
    fnorms[3*i+0] = nv.x; fnorms[3*i+1] = nv.y; fnorms[3*i+2] = nv.z;
  }

}

int main(int argc, char **argv) {
  //readObj("./box/WoodenBoxOpen02.obj");
  cout << "starting marching tetrahedrons ..." << endl;
  marchingTetrahedrons("./head.raw", 512, 512, 64, 2.0, 2.0, 1.0);
  cout << "marching tetrahedrons completed..." << endl;
  cout << "vertices: " << nfverts << endl;
  cout << "indeces : " << nfindeces << endl;

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(800, 600);
  glutCreateWindow("test1");
  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

  init();

  glutMainLoop();

  return 0;
}
